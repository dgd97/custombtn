export const isValidEmail = (strEmail) => {
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(strEmail))
}
export const isValidPhone = (strPhone) => strPhone.length == 10
export const isNumber = (string) => {
    return Number(string)
}

