import React, { useState } from "react";
import { TextInput, View, Text, Image } from "react-native";
import styles from "../button/_style";
import { isValidEmail, isValidPhone } from "../../utils/Validations";

export default function CustomTxtInput(
    {
        err = 'black',
        disable = true,
        val,
        size,
        fullWidth,
        startIcon,
        endIcon,
        helperText = '',
        row,
        isMultiline,
        label = '',
        secureText = false,
        value='',
        onChangeText,
        keyboardType
    }) {
    const getBorderColor = err === 'red' && 'red' || '#828282'
    const getBorder = () =>{
        
        if(label === 'pwdCA' || label === 'name'|| label === 'phone' || label === 'emailCA')
            return 1
    }
    const getColor = () => {
        if (err === 'red') return 'red'
        if (label === 'email' || label === 'pwd') return '#828282'
        return '#333333'
    }
    const getValue = val && 'text'
    const getSize = size === 'sm' && 40 || label === 'name' && 56 ||
        row && 150 || 56
    const getWidth = () => {
        if (fullWidth) return 400
        if ( label === 'emailCA' || label === 'phone' ||
            label === 'pwdCA' || label === 'name') return 350
        if( label === 'email' || label === 'pwd') return 375
        if( label === 'title' || label === 'content'
        ||label === 'startDate') return 325
 
        return 200
    }
    const getMagrinIcon = label === 'pwd' && 275
    const getToptxt = row && 'top'
    const getBgColor = disable && 'white' || '#E0E0E0'
    const getLabel = () => {
        if(label === 'email') return 'Email Address'
        if(label === 'pwd') return 'Password'
        if(label === 'name' || label === 'phone' ||
        label === 'emailCA' || label === 'pwdCA' ) return ' '
        if(label === 'title') return 'Title'
        if(label === 'content') return 'Content'
        if(label === 'startDate') return 'Start Date'

        return 'Label'
    } 
    const getMarginView =  label === 'email' && 20
    || label === 'pwd' && 20 || label === 'name' && 1
    || label === 'emailCA' && 1 || label === 'phone' && 1
    || label === 'pwdCA' && 1 || 10
    const getMarginTxt = label === 'email' && 20 || label === 'pwd' && 20 || 10
    const getPlaceHolder = () => {
        if (label === 'name') return 'Name'
        if (label === 'emailCA') return 'Email'
        if (label === 'pwdCA') return 'Password'
        if (label === 'phone') return 'Phone'
        if (label === 'title') return 'Plz enter a title'
        if (label === 'content') return 'Plz enter a content'
        if (label === 'startDate') return 'Plz pick a start date'

    }
    const getBold = () =>{
        if( label === 'title' || label ==='content' ||
        label === 'startDate') return 'bold'
        return 'normal'
    }
    const getMarginBot = () =>{
        if( label === 'title' || label ==='content' ||
        label === 'startDate') return 15
        return 0
    }
    const getSizeTxt = () =>{
        if( label === 'title' || label ==='content' ||
        label === 'startDate') return 15
        return 10
    }
    const getHeightTxt = () =>{
        if( label === 'title' || label ==='content' ||
        label === 'startDate') return 16
        return 15
    }

    // State
    const [errorEmail, setErrorEmail] = useState('')
    const [errorPhone, setErrorPhone] = useState('')
    const getValid = () => {
        if (label === 'email' || label === 'emailCA')
            setErrorEmail(isValidEmail(value) == true ? '' : 'Email not in correct format')
        if (label === 'phone')
            setErrorPhone(isValidPhone(value) == true ? '' : 'Phone number is 10 digits')
    }
    return (
        <View>
            <Text style={[styles.defaultTxt, { marginLeft: getMarginTxt,
                marginBottom:getMarginBot(),
                color: getColor(),
                fontSize:getSizeTxt(),
                height:getHeightTxt(),
                fontWeight:getBold() }]}>{getLabel()}</Text>
            <View style={[styles.iconView, {
                borderColor: getBorderColor,
                borderWidth:getBorder(),
                width: getWidth(),
                 height: getSize, 
                 backgroundColor: getBgColor,
                  marginLeft: getMarginView
            }]}>
                {startIcon && <Image style={[styles.iconTxtInput,]} source={require('../../assets/images/viber.png')} />}
                <TextInput style={[styles.defaultTxtInput, 
                { textAlignVertical: getToptxt }]}
                    placeholder={getPlaceHolder()}
                    placeholderTextColor={'#828282'}
                    editable={disable}
                    value={value}
                    multiline={isMultiline}
                    numberOfLines={4}
                    secureTextEntry={secureText}
                    onChangeText={onChangeText}
                    onEndEditing ={ () => getValid(value)}
                    keyboardType={keyboardType}
                />
                {endIcon === 'eye' && <Image style={[styles.endIconTxtInput, { marginLeft: getMagrinIcon }]} source={require('../../assets/images/eye.png')} /> ||
                    endIcon === 'lock' && <Image style={[styles.endIconTxtInput]} source={require('../../assets/images/lock.png')} />}
            </View>
            <Text style={[styles.defaultTxt, {width:170 , color: 'red' }]}>{
                label === 'emailCA' && errorEmail || errorPhone} </Text>
        </View>
    )
}