import React from "react";
import { Text, View } from "react-native";
import styles from "../button/_style";
import { useNavigation } from "@react-navigation/native";

export default function CustomTxt({label, detail }){
    const navigation = useNavigation()
    const getLabel = () => {

    if(label === 'forgotPwd' ) return 'Forgot Password?'
    if(label === 'haveAcc') return "Don't have an account?"
    if(label === 'register') return 'Register' 
    if(label === 'creAcc') return 'Create Account'
    if(label === 'alreadyHaveAcc') return "Already have an account?"
    if(label === 'notes') return "NOTES"
    if(label === 'title') return "CREATE A NEW TASK !!"
    if(label === 'startDate') return "Start date"
    if(label === 'logIn') return 'LogIn'

    return label
}
    const getColor = () => {
        if(label === 'haveAcc' || label === 'alreadyHaveAcc') return  '#828282'
        if(label === 'startDate') return 'black'
        if(label === 'UPDATE A TASK' || label === 'title'
            || label === 'notes') return '#0039CB'
        if(label === 'Title:' || label === 'Content:' ||
        label === 'Start Date:' || label === 'Process:') return 'black'
        return '#0039CB'
    }
    navigation
    const getSizeTxt = () =>{
        if(label === 'creAcc' ) return 20
        if(label === 'notes') return 30
        if(label === 'title' || label === 'UPDATE A TASK') return 17
        if(detail === 'title' || detail === 'content' || detail === 'startDate'
        || detail === 'doing' || detail === 'completed'
        || label === 'Title:' || label === 'Content:' ||
        label === 'Start Date:' || label === 'Process:') return 22
        return 15
    } 
    const getMargin = () => {
        if (label === 'logIn') return 5
        if(label === 'creAcc' ) return 25
        if( label === 'emailCA' || label === 'phone'
        || label === 'pwdCA' || label === 'name'
        || label === 'Title:' || label === 'Content:' ||
        label === 'Start Date:' || label === 'Process:') return 0
        if(label === 'title') return 0
        if(label === 'UPDATE A TASK' ) return 0
        if(label === 'notes' ) return 0
        if(detail === 'title' ) return 0
        return 10
    }
    const getTxtAlign = label ==='forgotPwd' && "center" || null
    const getMarginTop = () => {
        if(label === 'notes') return 70
        if(label === 'title' || label === 'UPDATE A TASK') return 50
        if(label === 'startDate') return 0.1
        return 10
    } 
    const marginRight = label === 'startDate' && 22 || 0
    const marginBot = label === 'startDate' && 0 || label === 'notes' && 20 || 0 
    const getWidth = () => {
        if(detail === 'title' || detail === 'content' ||
        detail === 'startDate' || detail === 'completed'
        || detail === 'doing') return 80
        if(label === 'Title:' || label === 'Content:' ||
         label === 'Process:') return 90
        if(label === 'notes') return 95
        if(label === 'Start Date:') return 110
        if(label === 'UPDATE A TASK') return 105
        if(label === 'startDate') return 55
        if(label === 'title') return 145
        return 0
    } 
    return(
        <View>
             <Text  style={[styles.customTxt,  {
                marginLeft: getMargin(),
                marginTop:getMarginTop(),
                marginRight:marginRight,
                marginBottom:marginBot,
                 color: getColor(), 
            fontSize:getSizeTxt(), 
            textAlign:getTxtAlign,
            width:getWidth()
        }]}
            onPress={  () => navigation.navigate('SignUp')}
    >{getLabel()}</Text>
        </View>
       
    )
}