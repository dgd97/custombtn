import { TouchableOpacity, Text, Image } from "react-native";
import React from "react";
import styles from "./_style";
import { useNavigation } from "@react-navigation/native";
const colorList = {
  txtColorDefault: '#3F3F3F',
  default: '#E0E0E0',
  noColor: null,
  white: 'white',
  blue: '#3D5AFE',
  primary: '#2962FF',
  primary1: '#0039CB',
  secondary: '#455A64',
  secondary1: '#1C313A',
  danger: '#D32F2F',
  danger1: '#9A0007',
  disableColor: '#9E9E9E',
  
}
export default function CustomBtn({
  variant = '', size = '', disableShadow = false, startIcon, endIcon, color = '',
  disable, logIn,onPress, sourceIcon, styleBtn
}) {
  const getBgColor = () => {
    if (variant === 'outline') return colorList.white
    if (variant === 'text' || size === 'btnBack'
    || size === 'btnEdit' || size === 'btnDel') return colorList.noColor
    if (variant === 'disableShadow') return colorList.blue
    if (color === 'secondary') return colorList.secondary
    if (color === 'danger') return colorList.danger
    if (color === 'secondary1') return colorList.secondary1
    if (color === 'primary1') return colorList.primary1
    if (color === 'danger1') return colorList.danger1
    if (logIn) return 'violet'
    if (disableShadow || startIcon || endIcon || size || color === 'primary') return colorList.primary
    
    return colorList.default

  }
  const getTxtColor = () => {
    if (variant === 'outline' || variant === 'text' && !disable) return colorList.blue
    if (variant === 'disableShadow') return colorList.white
    if (disable) return colorList.disableColor
    if (size === 'sm' || size === 'md' || size === 'lg' ||
    size === 'btnNew' || size === 'btnCreate' || size === 'btnUpdate') return colorList.white
    if (color === 'default') return colorList.textDefaut
    if (startIcon || endIcon) return colorList.white
    if (size === 'big') return 'white'
    if (color === 'primary' || color === 'secondary' || color === 'danger' ||
      color === 'primary1' || color === 'secondary1' || color === 'danger1')
      return colorList.white
      if (size === 'btnSignUp') return colorList.white
    return colorList.txtColorDefault
  }
  const sizeArr = {
    sm: {
      width: 73,
      height: 32,
    },
    md: {
      width: 81,
      height: 36,
    },
    lg: {
      width: 93,
      height: 42,
    },
    big:{
      width:375,
      height:60
    },
    btnSignUp:{
      width:350,
      height:40
    },
    btnBack:{
      width:50,
      height:25
    },
    btnNew:{
      width:70,
      height:70
    },
    btnEdit:{
      width:25,
      height:25
    },
    buttonDel:{
      width:25,
      height:25
    },
    btnCreate:{
      width:120,
      height:50
    },
    btnUpdate:{
      width:120,
      height:50
    }
  }
  const getSize = () => {
    return sizeArr[size]
  }
  const getBorder = variant === 'outline' && 1 || 0
  const btnLabel = disable && 'Disabled' || color === 'secondary' && 'Secondary' ||
    color === 'secondary1' && 'Secondary' || color === 'danger' && 'Danger' ||
    color === 'danger1' && 'Danger' || size === 'big' && 'LogIn' || 
    size === 'btnNew' && '+' || size === 'btnSignUp' && 'SIGN UP' ||
     size === 'btnBack' && ' '|| size ==='btnEdit' && ' ' ||
     size === 'btnDel' && ' '  || size ==='btnCreate' && 'CREATE' 
     || size === 'btnUpdate' && 'UPDATE' || 'Default' 
     
  
  const getWidth = () => {
    if(startIcon || endIcon ) return 105
    if(variant === 'signUp') return 350
    return 81
  } 
  const getWidthTxt = () => {
    if(color === 'secondary' || color === 'secondary1') return 72
    if(size === 'big') return 72
    if(size === 'btnSignUp' || size === 'btnCreate' || 
    size === 'btnUpdate') return 72
    return 49
  } 
 
  const getMargin = size === 'big' && 20 || size === 'hug' && 15 || 5
  const getFontWeight = size === 'btnSignUp' && 'bold' || size === 'big' && 'bold'
  || size ==='btnNew' && 'bold' || size === 'btnCreate' && 'bold'
  || size === 'btnUpdate' && 'bold' || 'normal'
  const getBorderRadius = size ==='btnNew' && 40 || 6
  const getFontSize = size === 'btnNew' && 40 || 15
  const getMarginLeft = size === 'btnNew' && 300 || 
  size === 'btnEdit' && 1 || 0
  const getMarginTop = size === 'btnNew' && 250 || size === 'btnCreate' 
  && 10|| 0.1
  
  return (
    <TouchableOpacity
     disabled={disable}
     onPress={onPress}
      style={[styles.buttonDefault, { backgroundColor: getBgColor(),
         borderWidth: getBorder,
       width: getWidth(), margin:getMargin,
       borderRadius:getBorderRadius,
       marginLeft:getMarginLeft,
       marginTop:getMarginTop,
      },  getSize(), styleBtn]}>
      {/* if(size === 'btnBack') <Image source={sourceIcon} /> */}
      {startIcon && <Image source={require('../../assets/images/trolley.png')} /> ||
      size ==='btnBack' && <Image style={{width:25, height:25}} source={sourceIcon} />
      || size === 'btnEdit' && <Image style={{width:25, height:25}} source={sourceIcon}/>
      || size === 'btnDel' && <Image style={{width:25, height:25}} source={sourceIcon}/>}
      <Text style={[styles.textDefaut, { color: getTxtColor(),
         width: getWidthTxt(),
          fontWeight:getFontWeight,
          fontSize:getFontSize }]}>
        {btnLabel}
      </Text>
      {endIcon && <Image source={require('../../assets/images/trolley.png')} />}
    </TouchableOpacity>

  );
}




