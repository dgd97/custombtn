import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    buttonDefault: {
        height:36,
        flexDirection: "row",
        alignItems:"center",
        justifyContent:"space-evenly",
        borderColor:'#3D5AFE',
    },
    textDefaut: {
        textAlign:'center',
        textAlignVertical:"center",
        height:60
        
    },
    Icon: {
        width:14,
        height:15,
    },
    defaultTxtInput:{
         marginLeft:10,
        
    },
    defaultTxt:{
        marginLeft:10,
        width:90,
        height:15,
        
    },
    iconView:{
        borderBottomWidth:1,
        borderRadius:8,
        flexDirection: "row", 
        alignItems:"center",
    },
    iconTxtInput:{
        width:14,
        height:15,
        marginLeft:10,
    },
    endIconTxtInput:{
        width:14,
        height:15,
        marginLeft:80
    },
    logInTxt:{
        marginLeft: 10,
        marginTop:50,
        marginBottom:30,
        fontSize:30,
        color:'black', 
    },
    customTxt:{
        height:32,
        fontWeight:"bold",
    },
    imgSignUp:{
        height:130,
        width:200,
        
    },
    imgLogoView:{
        alignItems:"center",
        marginTop:30,
    },
    imgBackIcon:{
        position:"absolute",

        left:20
    }


})
export default styles;
