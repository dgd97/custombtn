
import React from 'react';
import CustomBtn from '../src/components/button/ButtonComponent'
import CustomTxtInput from './components/textInput/txtInputComp';
import { ScrollView, View } from "react-native";
import RootComponent from './views/screens/index'
export default function App() {

  return (
    <RootComponent />

  //   <View>
  //     <CustomBtn />
  //     <CustomBtn variant='outline' />
  //     <CustomBtn variant='text' />
  //     <CustomBtn variant='disableShadow' />
  //     <View style={{ flexDirection: 'row' }}>
  //       <CustomBtn disable />
  //       <CustomBtn disable variant='text' />
  //     </View>
  //     <View style={{ flexDirection: 'row' }}>
  //       <CustomBtn startIcon='trolley' />
  //       <CustomBtn endIcon='trolley' />
  //     </View>
  //      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
  //       <CustomBtn size='sm' />
  //       <CustomBtn size='md' />
  //       <CustomBtn size='lg' />
  //     </View>
  //     <View style={{ flexDirection: 'row' }}>
  //       <CustomBtn color='default' />
  //       <CustomBtn color='primary' />
  //       <CustomBtn color='secondary' />
  //       <CustomBtn color='danger' />
  //     </View>
  //     <View style={{ flexDirection: 'row' }}>
  //       <CustomBtn color='primary1' />
  //       <CustomBtn color='secondary1' />
  //       <CustomBtn color='danger1' />  
  // </View> 
  //      <ScrollView>
  //       <CustomTxtInput></CustomTxtInput>
  //       <CustomTxtInput ></CustomTxtInput>
  //       <CustomTxtInput disable={false} ></CustomTxtInput>
  //       <CustomTxtInput helperText='Something interesting this' ></CustomTxtInput>
  //       <CustomTxtInput helperText='Something interesting this' err='red' ></CustomTxtInput>
  //       <CustomTxtInput startIcon={'viber'}  ></CustomTxtInput>
  //       <CustomTxtInput endIcon={'lock'}  ></CustomTxtInput>
  //       <CustomTxtInput  val={'text'} ></CustomTxtInput>
  //       <CustomTxtInput  size='sm' ></CustomTxtInput>
  //       <CustomTxtInput  size ='md' ></CustomTxtInput>
  //       <CustomTxtInput  fullWidth={"yes"}></CustomTxtInput>
  //       <CustomTxtInput isMultiline={true}  row={'4'}></CustomTxtInput>
  //     </ScrollView> 
  //   </View>
  )
}
