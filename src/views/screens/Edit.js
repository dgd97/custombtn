import React, { useState } from "react";
import { View, Image } from "react-native";
import CustomTxt from "../../components/text/CustomText";
import CustomTxtInput from "../../components/textInput/txtInputComp";
import CustomBtn from "../../components/button/ButtonComponent";
import DatePicker from "react-native-datepicker";
import AsyncStorage from "@react-native-async-storage/async-storage";
import SelectDropdown from 'react-native-select-dropdown'
import { FlatList } from "react-native-gesture-handler";

const processes = ["Doing", "Completed"]


const Edit = ( props) => {

  const { data, id, rec } = props.route.params;
  const [title, setTitle] = useState(rec.title)
  const [content, setContent] = useState(rec.content)
  const [date, setDate] = useState(rec.startDate)
  const [selected, setSelected] = useState(rec.isCompleted)
  
  const EditTask = async () => {

    data[id].title = title
    data[id].content = content
    data[id].startDate = date
    data[id].isCompleted = selected 
    
    await AsyncStorage.setItem('TASK', JSON.stringify(data))
    props.navigation.goBack()
  }
    return (
      <View style={{ alignItems: "center" }}>
        <CustomTxt label={'UPDATE A TASK'} />
        <Image style={{
          height: 20, width: 23,
          marginTop: 20,
          marginBottom: 20
        }} source={require('../../assets/images/todo.png')} />
        <CustomTxtInput label="title" value={title} onChangeText={setTitle} />
        <CustomTxtInput label="content" value={content} onChangeText={setContent} />
        <CustomTxt label='startDate' value={date} />
        <DatePicker style={{ width: 325, marginBottom: 10 }}
          date={rec.startDate}
          mode="date"
          placeholder="Plz select date"
          format="DD/MM/YYYY"
          minDate="01-01-1900"
          maxDate="01-01-2023"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              right: -5,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 15,
              borderColor: "black",
              alignItems: "flex-start",
              borderWidth: 0,
              borderBottomWidth: 0.5,
            },
            placeholderText: {
              fontSize: 17,
              color: "gray"
            },
            dateText: {
              fontSize: 17,
            }
          }}
          onDateChange={(date) => {
            setDate(date);
          }} />
        <SelectDropdown style={{ width: 200, height: 100,
         backgroundColor: 'blue' }}
          data={processes}
          defaultButtonText={'Select Process'}
          defaultValue={selected && 'Completed' || 'Doing'}
          value={selected}
          onSelect={ (selected) => {
            if(selected === 'Doing') {
              selected = false
              setSelected(selected)
            }
            else {selected = true
            setSelected(selected)}
            }} />
        <CustomBtn size="btnUpdate" onPress={() => EditTask()} />
      </View>
    )
}
export default Edit