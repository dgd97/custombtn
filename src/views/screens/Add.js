import React, { useState } from "react";
import { View, Image } from "react-native";
import CustomTxt from "../../components/text/CustomText";
import CustomTxtInput from "../../components/textInput/txtInputComp";
import CustomBtn from "../../components/button/ButtonComponent";
import DatePicker from "react-native-datepicker";
import AsyncStorage from "@react-native-async-storage/async-storage";

const Add = (props) => {
 
  const [title, setTitle] = useState()
  const [content, setContent] = useState()
  const [date, setDate] = useState()

  const saveTask = async () => {
    let tasks
     tasks = JSON.parse(await AsyncStorage.getItem('TASK'))
     if(tasks == null) tasks = []
     tasks.push({ title: title, content: content, startDate: date, isCompleted: false })
    await AsyncStorage.setItem('TASK', JSON.stringify(tasks))
    props.navigation.goBack()
    
  }
    return (
        <View style={{ alignItems:"center"}}>
            <CustomTxt label={'title'}/>
             <Image style={{height:50, width:56,
                 marginTop:20,
                 marginBottom:20
                 }} source={require('../../assets/images/todo.png')} />
            <CustomTxtInput label="title" value={title} onChangeText={setTitle}/>
            <CustomTxtInput label="content" value={content} onChangeText={setContent}/>
            <CustomTxt label={'startDate'}/>
            <DatePicker style={{width:325, marginBottom: 10}}
            date={date}
            mode="date"
            placeholder="Plz select date"
            format="DD/MM/YYYY"
            minDate="01-01-1900"
            maxDate="01-01-2023"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                right: -5,
                top: 4,
                marginLeft: 0,
              },
              dateInput: {
                marginLeft:15,
                borderColor : "black",
                alignItems: "flex-start",
                borderWidth: 0,
                borderBottomWidth: 0.5,
              },
              placeholderText: {
                fontSize: 17,
                color: "gray"
              },
              dateText: {
                fontSize: 17,
              }
            }}
            onDateChange={(date) => {
              setDate(date);
            }}/>
                 <CustomBtn size="btnCreate" onPress={() => saveTask()}/>
             </View>
    )
}
export default Add