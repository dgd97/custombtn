import React from "react";
import { Text } from "react-native";
import LogIn from "./Login";
import SignUp from "./SignUp";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "./Home";
import Doing from "./Doing";
import Finish from "./Finish";
import Detail from "./Detail";
import Add from "./Add";
import Edit from "./Edit";
export default RootComponent = () => {
    const Stack = createNativeStackNavigator();

    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="LogIn" component={LogIn} />
                <Stack.Screen name="SignUp" component={SignUp} />
                <Stack.Screen name="Add" component={Add} />
                <Stack.Screen name="Edit" component={Edit} />
                <Stack.Screen name="Doing" component={Doing} />
                <Stack.Screen name="Finish" component={Finish} />
                <Stack.Screen name="Detail" component={Detail} />
            </Stack.Navigator>
        </NavigationContainer>

    )
}