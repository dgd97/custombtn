import React from "react";
import { View, Text } from "react-native";
import styles from "../../components/button/_style";
import CustomTxtInput from "../../components/textInput/txtInputComp";
import CustomBtn from "../../components/button/ButtonComponent";
import CustomTxt from "../../components/text/CustomText";
export default function LogIn ({navigation}){
    return(
        <View style={{backgroundColor:"white", flex: 1}}>
            <Text style={[styles.logInTxt]}> Login </Text>
            <CustomTxtInput label="email"/>
            <CustomTxtInput label="pwd" endIcon={'eye'} secureText={true}/>
            <CustomBtn size="big"/>
            <CustomTxt label='forgotPwd'></CustomTxt>
            <View style={{ flexDirection:"row", justifyContent:"center", marginLeft:100}}>
            <CustomTxt label='haveAcc'/>
            <CustomTxt label='register'  navigation={navigation}/>
            </View>
        </View>
    )
}