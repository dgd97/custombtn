import React, {useState } from "react";
import { View, Image, TouchableOpacity, Alert, KeyboardAvoidingView } from "react-native";
import styles from "../../components/button/_style";
import CustomTxt from "../../components/text/CustomText";
import CustomTxtInput from "../../components/textInput/txtInputComp";
import CustomBtn from "../../components/button/ButtonComponent";
export default function SignUp(props) {
    // console.log("props", props.navigation.goBack())
    
    const [name, setName] = useState()
    const [email, setEmail] = useState()
    const [phone, setPhone] = useState()
    const [passWord, setPassWord] = useState()
    // const onChangeText = (key, text) => {
    //     setCreateAcc(prevState=> ({
    //         ...prevState,
    //         prevState[key]: text
    //     }))

    // }

    const onSubmit = () => {
        if (name && email && phone && passWord) props.navigation.goBack()
        else return Alert.alert("U gotta fill full fields")

    }
    return (
        <KeyboardAvoidingView
        style={{flex: 1}}
        >
<View style={[styles.imgLogoView]}>
          <TouchableOpacity
           style={[styles.imgBackIcon]} onPress={() => props.navigation.goBack()} >
            <Image style={{width:25, height:25}} source={require('../../assets/images/arrow.png')}/>
            </TouchableOpacity>
             

            <Image style={[styles.imgSignUp]} source={require('../../assets/images/logo.png')} />
            <CustomTxt label='creAcc'></CustomTxt>
            <CustomTxtInput label="name" value={name} onChangeText={setName} />
            <CustomTxtInput label="emailCA" value={email} onChangeText={setEmail} />
            <CustomTxtInput label="phone" value={phone} onChangeText={setPhone}
             keyboardType={Platform.OS === 'android' ? "numeric" : "number-pad"}
            />
            <CustomTxtInput label="pwdCA" secureText={true} value={passWord} onChangeText={setPassWord} />
            <CustomBtn size="btnSignUp" onPress={onSubmit} />
            <View style={{ flexDirection: "row", marginLeft: 100 }}>
                <CustomTxt label='alreadyHaveAcc'></CustomTxt>
                <CustomTxt label='logIn'></CustomTxt>
            </View>
        </View>
        </KeyboardAvoidingView>
        
    )
}