import React, { useEffect, useState } from "react";
import { View, FlatList, Text, useWindowDimensions } from "react-native";
import CustomTxt from "../../components/text/CustomText";
import CustomBtn from "../../components/button/ButtonComponent";
import { TabView, SceneMap } from "react-native-tab-view";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useIsFocused } from "@react-navigation/native";

export default function Home(props) {
  const onSubmit = () => {
    props.navigation.navigate('Add')
  }
  const detail = item => {
    props.navigation.navigate('Detail', { rec: item })
  }
  const [taskList, setTaskList] = useState([])
  const [doingList, setDoingList] = useState([])
  const [completedList, setCompletedList] = useState([])

  const isFocused = useIsFocused()

  useEffect(() => {
    getData()     //  load data 
  }, [isFocused])

  const getData = async () => {
    const task = await AsyncStorage.getItem('TASK')
    setTaskList(JSON.parse(task))
    let tempTask = []
    let doingTask = []
    let completedTask = []
    console.log(task)
    if (task) {
      tempTask = JSON.parse(task);
      tempTask.forEach(e => {
        if (e.isCompleted) completedTask.push(e)
        else doingTask.push(e)
      });
      setCompletedList(completedTask)
      setDoingList(doingTask)
    }

  }

  const delItem = async index => {
    let newTask = JSON.parse(await AsyncStorage.getItem('TASK'))
    if(newTask[index].isCompleted){
    newTask.splice(index, 1)
    await AsyncStorage.setItem('TASK', JSON.stringify(newTask))
    setTaskList(newTask)
    setCompletedList(newTask)
    }
    newTask.splice(index, 1)
    await AsyncStorage.setItem('TASK', JSON.stringify(newTask))
    setTaskList(newTask)
    setDoingList(newTask)
  }


  const Item = ({ item, index }) => (
    <View style={{
      backgroundColor: 'white',
      flexDirection: "row",
    }}>
      <Text style={{ fontSize: 22, width:180, textAlignVertical:"center",paddingLeft:10 }}
        onPress={() => detail(item)}>{item.title}</Text>
      <View style={{ flexDirection: "row", alignItems:"center", marginLeft: 70}}>
        <CustomBtn 
        onPress={() => props.navigation.navigate('Edit', { data: taskList, id: index, rec: item })}
          size="btnEdit" sourceIcon={require('../../assets/images/draw.png')} />
        <CustomBtn onPress={() => delItem(index)}
          size="btnDel" sourceIcon={require('../../assets/images/delete.png')} />
      </View>
    </View>
  );

  const renderItem = ({ item, index }) => (
    <Item item={item} index={index} />
  );

  const FirstRoute = () => (
    <View style={{ flex: 1, alignItems:"center" }}>
      <CustomTxt label='notes' />
      <FlatList data={taskList}
        renderItem={renderItem}
      />
      <CustomBtn size='btnNew' styleBtn={{ position: 'absolute', bottom: 50,
       right: 30 }} onPress={onSubmit} />
    </View>
  )
  const SecondRoute = () => (
    <View style={{flex: 1, alignItems:"center"}}>
      <CustomTxt label='notes' />
      <FlatList data={doingList}
        renderItem={renderItem}
      />
    </View>);
  const ThirdRoute = () => (
    <View style={{flex: 1, alignItems:"center"}}>
      <CustomTxt label='notes' />
      <FlatList data={completedList}
        renderItem={renderItem}
      />

    </View>);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute
  });
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'Home' },
    { key: 'second', title: 'Doing' },
    { key: 'third', title: 'Completed' },
  ]);

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{ width: layout.width }}
    />
  );
}