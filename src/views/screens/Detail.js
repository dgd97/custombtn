import React from "react";
import { View, Image } from "react-native";
import CustomTxt from "../../components/text/CustomText";
export default function Detail(props) {
    const { rec } = props.route.params
    return (
        <View style={{
            flex: 1,
            alignItems: "center"
        }}>
            <Image style={{
                height: 150, width: 200,
                marginTop: 50,
                marginBottom: 20,
            }} source={require('../../assets/images/Tasks.png')} />
            <View>
            <View style={{ flexDirection: "row", }}>
                    <CustomTxt label={'Title:'} />
                    <CustomTxt label={rec.title} detail='title' />
                </View>
                <View style={{ flexDirection: "row" }}>
                    <CustomTxt label={'Content:'} />
                    <CustomTxt label={rec.content} detail='content' ></CustomTxt>
                </View>
                <View style={{ flexDirection: "row" }}>
                    <CustomTxt label={'Start Date:'} />
                    <CustomTxt label={rec.startDate} detail='startDate'  ></CustomTxt>
                </View>
                <View style={{ flexDirection: "row" }}>
                    <CustomTxt label={'Process:'} />
                    {rec.isCompleted && <CustomTxt label={'Completed'} detail='completed'  ></CustomTxt>
                        || <CustomTxt label={'Doing'} detail='doing' ></CustomTxt>}
                </View>
            </View>




        </View>
    )
}